
public class TiposDatos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num1 = 100;
		
		// Castin de variables
		
		short num1Cast = (short)num1;
		
		// num1Cast se puede concatenar dado que es char
		
		System.out.println("Valor casteado "+num1Cast);
		
		char letra = '5';
		
		int numero = (int)letra;
		
		System.out.println(numero);
	}

}
